/*******************************************************************************
 * Copyright 2011 sikuli.org
 * Released under the MIT license.
 * 
 * Contributors:
 *     Tom Yeh - initial API and implementation
 ******************************************************************************/
package org.sikuli.makerbot;

import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_imgproc.*;

import java.awt.image.BufferedImage;

import org.sikuli.core.cv.VisionUtils;

import com.googlecode.javacpp.Loader;
import com.googlecode.javacv.cpp.opencv_core.*;

public class DefaultPrintableGenerator implements PrintableGenerator {
	static protected CvScalar getColorForLevel(int level){		
		CvScalar color = CvScalar.RED;		
		double factor = (1.0 - 0.2*level);
		color.red(255*factor);		
		return color;
	}


	protected void processPolygon(CvSeq polygon_points, IplImage image, int level){
		//		int total = polygon_points.total();
		//		System.out.print("> ");
		//		for (int i = 0; i < total; i++) {	
		//			//CvPoint points.
		//			CvPoint p = new CvPoint(cvGetSeqElem(polygon_points, i));
		//			System.out.print("(" + p.x() + "," + p.y() + ")");
		//			//cvPoints[i] = p;
		//
		//		}
		//		System.out.println("");
	}

	private void listContoursAtLevel(CvSeq contour, IplImage image, int level){

		if (contour == null || contour.isNull()) {
			return;	
		}

		CvMemStorage storage = CvMemStorage.create();

		while (contour != null && !contour.isNull()) {
			if (contour.elem_size() > 0) {

				
				CvRect rect = cvBoundingRect(contour, 0);
				
				CvSeq polygon_points = cvApproxPoly(contour, Loader.sizeof(CvContour.class),
						storage, CV_POLY_APPROX_DP, cvContourPerimeter(contour)*0.005, 0);

				
				//if (rect.height() > 20 && rect.width() > 20){
					
				
					processPolygon(polygon_points, image, level);
				//}
			}


			listContoursAtLevel(contour.v_next(), image, level + 1);

			contour = contour.h_next();
		}

	}

	private void listContours(CvSeq contour,  IplImage image){
		listContoursAtLevel(contour, image, 0);
	}

	public Printable createFrom(BufferedImage input) {
		IplImage image = IplImage.createFrom(input);
		IplImage foreground = VisionUtils.computeForegroundMaskOf(image);

		CvMemStorage storage = CvMemStorage.create();		
		int contour_method = CV_RETR_TREE;		
		CvSeq contour = new CvSeq(null);
		cvFindContours(foreground, storage, contour, Loader.sizeof(CvContour.class),
				contour_method, CV_CHAIN_APPROX_SIMPLE);


		// create a C3 gray image that can be colorfully annotated
		IplImage gray = VisionUtils.createGrayImageFrom(image);
		IplImage gray3 = IplImage.create(cvGetSize(gray), 8, 3);     
		cvCvtColor(gray, gray3, CV_GRAY2RGB);

		//IplImage preview = gray3;
		IplImage preview = IplImage.create(cvGetSize(image),8,3);
		cvSet(preview, CvScalar.WHITE, null);

		listContours(contour, preview);

		return new DefaultPrintable(preview.getBufferedImage());
	}
}
