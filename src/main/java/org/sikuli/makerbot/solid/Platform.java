/*******************************************************************************
 * Copyright 2011 sikuli.org
 * Released under the MIT license.
 * 
 * Contributors:
 *     Tom Yeh - initial API and implementation
 ******************************************************************************/
package org.sikuli.makerbot.solid;

import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_imgproc.*;
import com.googlecode.javacpp.Loader;
import com.googlecode.javacv.cpp.opencv_core.*;

import org.stringtemplate.v4.ST;


public class Platform extends AbstractSolid {	
	private final int width;
	private final int height;

	public Platform(int width, int height) {
		super();
		this.width = width;
		this.height = height;
	}
	
	public String toSCAD(){		
		ST st = g.getInstanceOf("platform");
		st.add("width",width);
		st.add("height",height);
		return st.render();
	}

}
