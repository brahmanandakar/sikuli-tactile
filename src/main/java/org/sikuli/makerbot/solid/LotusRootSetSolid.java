package org.sikuli.makerbot.solid;

import static com.googlecode.javacv.cpp.opencv_core.CV_AA;
import static com.googlecode.javacv.cpp.opencv_core.cvDrawContours;
import static com.googlecode.javacv.cpp.opencv_core.cvGetSize;
import static com.googlecode.javacv.cpp.opencv_core.cvScalarAll;
import static com.googlecode.javacv.cpp.opencv_core.cvSet;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_CHAIN_APPROX_SIMPLE;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_GRAY2BGR;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_RETR_TREE;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_SHAPE_RECT;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_THRESH_BINARY_INV;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvCvtColor;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvDilate;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvFindContours;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvThreshold;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import org.sikuli.core.cv.ImagePreprocessor;
import org.sikuli.core.logging.ImageExplainer;

import com.google.common.collect.Lists;
import com.googlecode.javacpp.Loader;
import com.googlecode.javacv.cpp.opencv_core.CvContour;
import com.googlecode.javacv.cpp.opencv_core.CvMemStorage;
import com.googlecode.javacv.cpp.opencv_core.CvScalar;
import com.googlecode.javacv.cpp.opencv_core.CvSeq;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import com.googlecode.javacv.cpp.opencv_imgproc.IplConvKernel;

public class LotusRootSetSolid extends AbstractSolid implements Solid {
	
	final static private ImageExplainer explainer = ImageExplainer.getExplainer(LotusRootSetSolid.class);
		
	final Platform platform;
	private LotusRootSetSolid(List<LotusRoot> lotusRootList, Platform platform) {
		super();
		this.platform = platform;
		this.lotusRootList = lotusRootList;
	}

	@Override
	public String toSCAD(){
		String scad = "";		
		scad += "union(){";
		scad += platform.toSCAD();
		for (LotusRoot model : lotusRootList){
			scad += model.toSCAD();
		}
		scad += "}";
		return scad;
	}
	
	final List<LotusRoot> lotusRootList;
	
	static class Preprocessor {
		public IplImage process(BufferedImage input){
			int dilation = 1;
			IplImage gray = ImagePreprocessor.createGrayscale(input);			
			IplImage foreground = IplImage.create(cvGetSize(gray), 8, 1);
			cvThreshold(gray,foreground,64, 255, CV_THRESH_BINARY_INV);
			IplConvKernel kernel = IplConvKernel.create(3,3,1,1,CV_SHAPE_RECT,null);
			cvDilate(foreground,foreground,kernel,dilation);
			//cvErode(foreground,foreground,kernel,dilation);
			return foreground;
		}
	}
	
	static private List<LotusRoot> collectParts(CvSeq contourSeq){
		List<LotusRoot> accumList = new ArrayList<LotusRoot>();	
		while (contourSeq != null && !contourSeq.isNull()){
			collectPartsHelper(contourSeq, accumList);
			contourSeq = contourSeq.h_next();
		}
		return accumList;		
	}

	static private void collectPartsHelper(CvSeq contourSeq, List<LotusRoot> accumList){
			LotusRoot part = createLotusRootFrom(contourSeq);			
			accumList.add(part);

			for (Contour hole : part.getHoles()){

				CvSeq nextLevel = hole.getCvSeq().v_next();
				while (nextLevel != null && !nextLevel.isNull()){				
					collectPartsHelper(nextLevel, accumList);
					nextLevel = nextLevel.h_next();
				}

			}
	}

	static private LotusRoot createLotusRootFrom(CvSeq contourSeq){

		if (contourSeq == null || contourSeq.isNull()) {
			return null;	
		}

		Contour boundaryContour = Contour.createFrom(contourSeq);
		LotusRoot model = new LotusRoot(boundaryContour);

		CvSeq holeSeq = contourSeq.v_next();
		while (holeSeq != null && !holeSeq.isNull()){
			model.getHoles().add(Contour.createFrom(holeSeq));
			holeSeq = holeSeq.h_next();
		}

		return model;
	}
	
	static Solid createFrom(BufferedImage input){

		Preprocessor pp = new Preprocessor();
		IplImage foreground = pp.process(input);

		explainer.step(input, "input");
		explainer.step(foreground, "foreground");

		CvMemStorage storage = CvMemStorage.create();		
		int contour_method = CV_RETR_TREE;		
		CvSeq contourSeq = new CvSeq(null);
		cvFindContours(foreground, storage, contourSeq, Loader.sizeof(CvContour.class),
				contour_method, CV_CHAIN_APPROX_SIMPLE);

		IplImage canvas = IplImage.create(cvGetSize(foreground),8,3);
		cvCvtColor(foreground, canvas, CV_GRAY2BGR);
		cvDrawContours(canvas, contourSeq, CvScalar.RED, CvScalar.BLUE, 10, 1, CV_AA);
		
		explainer.step(canvas, "contours");
		
		List<LotusRoot> models = collectParts(contourSeq);

		IplImage blank = IplImage.create(cvGetSize(foreground),8,3);
		cvSet(blank,cvScalarAll(0),null);
		for (LotusRoot model : models){
			model.paintOn(blank);
		}

		explainer.step(blank, "model 2D projection");
		
		Platform pt = new Platform(input.getWidth(), input.getHeight());
		LotusRootSetSolid m = new LotusRootSetSolid(models, pt);
		return m;
	}
}
