/*******************************************************************************
 * Copyright 2011 sikuli.org
 * Released under the MIT license.
 * 
 * Contributors:
 *     Tom Yeh - initial API and implementation
 ******************************************************************************/
package org.sikuli.makerbot;

import java.awt.image.BufferedImage;

public class DefaultPrintable implements Printable{

	public DefaultPrintable(BufferedImage image) {
		this.image = image;
	}

	final private BufferedImage image;

	public BufferedImage getImage(){
		return image;
	}
}
