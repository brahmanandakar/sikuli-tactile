/*******************************************************************************
 * Copyright 2011 sikuli.org
 * Released under the MIT license.
 * 
 * Contributors:
 *     Tom Yeh - initial API and implementation
 ******************************************************************************/
package org.sikuli.makerbot;

import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_imgproc.*;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;
import org.sikuli.core.cv.ImagePreprocessor;
import org.sikuli.core.cv.VisionUtils;
import org.sikuli.core.logging.ImageExplainer;
import org.sikuli.makerbot.ElevationMapPrintableGenerator;
import org.sikuli.makerbot.PolygonSetPrintableGenerator;
import org.sikuli.makerbot.Printable;
import org.sikuli.makerbot.PrintableGenerator;
import org.sikuli.makerbot.solid.Contour;
import org.sikuli.makerbot.solid.LotusRoot;
import org.sikuli.makerbot.solid.LotusRootSetSolid;
import org.sikuli.makerbot.solid.Platform;
import org.sikuli.makerbot.solid.Solid;
import org.sikuli.makerbot.solid.SolidFactory;

import com.googlecode.javacpp.Loader;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import com.googlecode.javacv.cpp.opencv_imgproc.IplConvKernel;

public class PrintableGeneratorTest {

	@Before
	public void setUp() throws Exception {
	}

	//void processContourAtEvenLevel(CvSeq contour, int level){

	//}

	//private void renderComponent



	ImageExplainer vlog = new ImageExplainer("debug");
	//@Test
	public void testRenderBySurface() throws IOException{

		ImageExplainer explainer = ImageExplainer.getExplainer("test");
		
		PrintStream out = new PrintStream(new FileOutputStream(
		"surface.dat"));

		//BufferedImage input = ImageIO.read(new File("src/test/resources/cell.png"));
		//BufferedImage input = ImageIO.read(new File("src/test/resources/cell-small.png"));
		BufferedImage input = ImageIO.read(new File("src/test/resources/obamabw.png"));

		
		int dilation = 1;
		
		
		IplImage gray = ImagePreprocessor.createGrayscale(input);
		IplImage foreground = VisionUtils.computeForegroundMaskOf(gray);
		
		vlog.log("foregroud",foreground.getBufferedImage());
		vlog.log("gray",gray.getBufferedImage());

		ByteBuffer buf = foreground.getByteBuffer();
		//IntBuffer buf = foreground.getIntBuffer();
		int pos = 0;
		while (buf.hasRemaining()){

			//Byte s = buf.get();

			Byte s = buf.get();
			//s = buf.get();
			//s = buf.get();

			//System.out.println("[" + s + "]");

			if (s == -1)
				out.print("1 ");
			else
				out.print("0 ");


			pos = pos + 1;
			if (pos == input.getWidth()){
				//if (pos == input.getHeight()*2){
				pos = 0;
				out.println();
			}

		}



	}
	
	


	@Test
	public void testRender() throws IOException{

		ImageExplainer explainer = ImageExplainer.getExplainer(getClass());
		explainer.setLevel(ImageExplainer.Level.STEP);
		
		//String inputImageFilename = "brain.png";
		String inputImageFilename = "atom1.png";
		//String inputImageFilename = "atom2.png";
//		String inputImageFilename = "cell.png";
//		String inputImageFilename = "volcano.png";
		
		BufferedImage input = ImageIO.read(new File("src/test/resources/", inputImageFilename));
		
		Solid solid = SolidFactory.createLotusRootSetSolidModel(input);		
		
		System.out.println(solid.toSCAD());
	}
	
	//@Test
//	public void testRenderSinlgeLevelBySubtraction() throws IOException{
//
//		//BufferedImage input = ImageIO.read(new File("src/test/resources/obamabw.png"));
//		//BufferedImage input = ImageIO.read(new File("src/test/resources/cell.png"));
//		//BufferedImage input = ImageIO.read(new File("src/test/resources/volcano.png"));
//		BufferedImage input = ImageIO.read(new File("src/test/resources/brain.png"));
//		//BufferedImage input = ImageIO.read(new File("src/test/resources/atom2.png"));
//		IplImage image = IplImage.createFrom(input);
//
//		IplImage foreground = VisionUtils.computeForegroundMaskOf(image);
//		//IplImage foreground = Utils.createGrayImageFrom(image);
//
//		vlog.log("foregroud",foreground.getBufferedImage());
//
//		CvMemStorage storage = CvMemStorage.create();		
//		int contour_method = CV_RETR_TREE;		
//		CvSeq contourSeq = new CvSeq(null);
//		cvFindContours(foreground, storage, contourSeq, Loader.sizeof(CvContour.class),
//				contour_method, CV_CHAIN_APPROX_SIMPLE);
//		
//
//		List<LotusRoot> models = collectParts(contourSeq);
//
//		IplImage blank = IplImage.create(cvGetSize(image),8,3);
//		cvSet(blank,cvScalarAll(0),null);
//
//		System.out.println("union(){");
//
//		Platform pt = new Platform(image.width(),image.height());
//		System.out.println(pt.toSCAD());
//
//		for (LotusRoot model : models){
//			model.paintOn(blank);
//			//if (model.getHoles().size() > 2){
//				System.out.println(model.toSCAD());
//			//}			
//		}
//		
//		//System.out.println(models.get(1).toSCAD());
//		
//		System.out.println("}");
//		
//		
//
//		vlog.log("lotusroot", blank.getBufferedImage());
//
//
//	}


	//@Test
	public void testCellImage() throws IOException{





		BufferedImage inputImage = ImageIO.read(new File("src/test/resources/cell.png"));
		{
			//System.out.println("union(){");

			PrintableGenerator pg = new PolygonSetPrintableGenerator();
			Printable p = pg.createFrom(inputImage);
			ImageIO.write(p.getImage(), "png", new File("output-polygon-set.png"));



			//System.out.println("}");
		}

		{

			//System.out.println("union(){");

			PrintableGenerator pg = new ElevationMapPrintableGenerator();
			Printable p = pg.createFrom(inputImage);

			ImageIO.write(p.getImage(), "png", new File("output-elevation-map.png"));

			//System.out.println("}");
		}


	}
}
